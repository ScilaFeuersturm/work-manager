package com.priscilagomez.workmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.work.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.btn_one_time_request)
        val text = findViewById<TextView>(R.id.tv_one_time_request)
        val periodic = findViewById<TextView>(R.id.btn_periodic_request)



        btn.setOnClickListener{
            val oneTimeConstraint  = Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build()

            val data = Data.Builder()
            data.putString("a", "awewafaf")


            val sampleWork = OneTimeWorkRequest
                .Builder(OneTimeRequestWorker::class.java)
                .setInputData(data.build())
                .setConstraints(oneTimeConstraint)
                .build()

            WorkManager.getInstance(this).enqueue(sampleWork)
            WorkManager.getInstance(this).getWorkInfoByIdLiveData(sampleWork.id)
                .observe(this) {
                    OneTimeRequestWorker.logger(it.state.name)
                    if(it != null){
                        when(it.state){
                            WorkInfo.State.ENQUEUED -> {
                                text.text = "Enqueued"
                            }
                            WorkInfo.State.BLOCKED -> {
                                text.text = "Blocked"
                            }
                            WorkInfo.State.RUNNING -> {
                                text.text = "Running"
                            }
                            WorkInfo.State.SUCCEEDED -> {
                                text.text = "Succeeded"
                            }
                        }
                    }
                }

            periodic.setOnClickListener {
                val periodicConstraint  = Constraints.Builder()
                    .setRequiresCharging(false)
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .setRequiresBatteryNotLow(true)
                    .build()


                val sampleWorkP = PeriodicWorkRequest
                    .Builder(PeriodicRequestWorker::class.java, 5 , TimeUnit.SECONDS)
                    .setConstraints(periodicConstraint)
                    .build()


                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    "Periodic",
                            ExistingPeriodicWorkPolicy.KEEP,
                            sampleWorkP
                )
            }

        }


    }
}