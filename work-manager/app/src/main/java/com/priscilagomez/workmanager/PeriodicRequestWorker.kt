package com.priscilagomez.workmanager

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.text.SimpleDateFormat
import java.util.*

class PeriodicRequestWorker(context : Context, worker: WorkerParameters) : Worker(context, worker) {
    override fun doWork(): Result {
        val data = getDate(System.currentTimeMillis())

        Log.e("Time", "$data")

        return Result.success()
    }

    private fun getDate(milliseconds : Long) : String {

        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS", Locale.getDefault())

        val calendar = Calendar.getInstance()

        calendar.timeInMillis = milliseconds

        return formatter.format(calendar.time)
    }
}