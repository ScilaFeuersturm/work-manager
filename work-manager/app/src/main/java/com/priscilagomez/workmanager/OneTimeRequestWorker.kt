package com.priscilagomez.workmanager

import android.content.Context
import android.util.Log
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters

class OneTimeRequestWorker(context : Context, params : WorkerParameters) : Worker(context, params){
    override fun doWork(): Result {
        val inputValue = inputData.getString("a")
        Log.e("Worker input", "$inputValue")

        //Do things

        return Result.success(createOutoputData())
    }


    private fun createOutoputData() : Data{
        return Data.Builder().putString("output", "lalalalala").build()
    }
    companion object{
        fun logger(message : String) = Log.i("Status", message)
    }
}